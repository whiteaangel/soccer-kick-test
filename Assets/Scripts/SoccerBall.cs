using System.Collections;
using Stickman;
using UI;
using UnityEngine;
public class SoccerBall : MonoBehaviour
{
    [SerializeField] private ForceBarScript forceBar;
    [SerializeField] private StickmanMain stickmanMain;
    [SerializeField] private GameObject cameraBall;
    [SerializeField] private GameObject cameraStand;
    [SerializeField] private GameObject fire;
    [SerializeField] private GameObject gamePanel;
    [SerializeField] private GameObject goal;
    
    private bool isFlying;
    private bool isControl;
    private float startForce;
    private Rigidbody rb;
    
    private float lastTime;

    private Touch touch;
    private const float SpeedModifier = 0.0003f;

    private float positionY;

    private void OnEnable()
    {
        forceBar.MMethodToCall += GetForce;
        stickmanMain.StartBall += StartBall;
    }

    private void OnDisable()
    {
        forceBar.MMethodToCall -= GetForce;
        stickmanMain.StartBall -= StartBall;
    }

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
        lastTime = Time.realtimeSinceStartup;
    }

    private void GetForce(float force)
    {
        startForce = force;
    }

    private void OnTriggerEnter(Collider other)
    {
        switch (isFlying)
        {
            case true when other.CompareTag("Ground"):
                isFlying = false;
                StartCoroutine(OnGround());
                break;
            case true when other.CompareTag("Enemy") && startForce < 5f:
                isFlying = false;
                rb.useGravity = true;
                rb.velocity = Vector3.zero;
                StartCoroutine(OnGround());
                break;
            case true when other.CompareTag("Enemy") && startForce >= 5f :
                if(!other.GetComponent<StickmanDefender>())
                    return;
                other.GetComponent<StickmanDefender>().GetDamage();
                break;
        }
    }

    private void StartBall()
    {
        cameraBall.SetActive(true);
        cameraStand.SetActive(false);
        
        Time.timeScale = 0.05f;
        Time.fixedDeltaTime = Time.timeScale * 0.02f;

        rb.isKinematic = false;

        var pos = (goal.transform.position - transform.position).normalized;
        pos += new Vector3(0, 1, 0);
        rb.AddForce(pos * startForce, ForceMode.Impulse);
       
        if(startForce >= 5f)
        {
            fire.SetActive(true);
        }
        isFlying = true;

        StartCoroutine(GravityOff());
    }
    
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.A))
        {
            Debug.Log("AAAAA");
        }
        if (startForce >= 5f)
        {
            var deltaTime = Time.realtimeSinceStartup - lastTime;
            fire.GetComponent<ParticleSystem>().Simulate (deltaTime, true, false);
            lastTime = Time.realtimeSinceStartup;
        }
        
        if (!isFlying) return;
        
        transform.RotateAround(transform.position, transform.up, Time.deltaTime * 90f);
        
        if(!isControl)
            return;
        transform.Translate(Vector3.back * Time.deltaTime * startForce);

        AndroidControl();
        EditorControl();
    }


    private void AndroidControl()
    {
        if (Input.touchCount <= 0) return;
        touch = Input.GetTouch(0);
        if (touch.phase != TouchPhase.Moved) return;
        
        //transform.position.x - touch.deltaPosition.x * SpeedModifier
        transform.position = new Vector3(transform.position.x,
            transform.position.y + touch.deltaPosition.y * SpeedModifier, transform.position.z);

        var rotationY = Quaternion.Euler(0, touch.deltaPosition.x * 0.05f, 0);
        transform.rotation = rotationY * transform.rotation;
    }

    private void EditorControl()
    {
        if(Input.GetKey(KeyCode.W)) 
        {
            if(transform.position.y > positionY)
                return;
            transform.position += Vector3.up * Time.deltaTime * 5f;
        }
        else if(Input.GetKey(KeyCode.S)) 
        {
            Debug.Log("S");
            transform.position -= Vector3.up * Time.deltaTime * 5f;
        }
        else if(Input.GetKey(KeyCode.A)) 
        {
            var rotationY = Quaternion.Euler(0,  Time.deltaTime * 5f, 0);
            transform.rotation = rotationY * transform.rotation;
            //transform.position += Vector3.right * Time.deltaTime * 5f;
        }
        else if(Input.GetKey(KeyCode.D)) 
        {
            var rotationY = Quaternion.Euler(0,  -Time.deltaTime * 5f, 0);
            transform.rotation = rotationY * transform.rotation;
            //transform.position += Vector3.left * Time.deltaTime * 5f;
        }
    }

    private  IEnumerator OnGround()
    {
        yield return new WaitForSeconds(0.1f);
        Time.timeScale = 0f;
        gamePanel.SetActive(true);
        gamePanel.GetComponent<GamePanel>().SetText("You lose!");
    }
    
    private IEnumerator GravityOff()
    {
        yield return new WaitForSeconds(0.1f);
        rb.useGravity = false;
        
        positionY = transform.position.y;

        rb.velocity = Vector3.zero;
        rb.angularVelocity = Vector3.zero;

        isControl = true;
    }

}
