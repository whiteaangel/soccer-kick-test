using System;
using System.Collections;
using UnityEngine;

namespace Stickman
{
    public class StickmanMain : MonoBehaviour
    {
        [SerializeField] private Animator animator;
        [SerializeField] private ForceBarScript forceBar;

        private static readonly int Kick = Animator.StringToHash("Kicking");
        public event Action StartBall;
        private void OnEnable()
        {
            forceBar.KickBall += KickBall;
        }

        private void OnDisable()
        {
            forceBar.KickBall -= KickBall;
        }

        private void KickBall()
        {
            StartCoroutine(AnimationWait());
            animator.SetTrigger(Kick);
        }

        private IEnumerator AnimationWait()
        {
            yield return new WaitForSeconds(0.8f);
            StartBall?.Invoke();
        }
    }
}
