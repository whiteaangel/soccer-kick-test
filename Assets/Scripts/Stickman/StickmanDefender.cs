using System;
using System.Collections;
using UnityEngine;

namespace Stickman
{
   public class StickmanDefender : MonoBehaviour
   {
      [SerializeField] private GameObject root;
      [SerializeField] private Transform overlapTransform;
      [SerializeField] private LayerMask ballLayer;
      [SerializeField] private Animator animator;
      [SerializeField] private Rigidbody rb;
      [SerializeField] private CapsuleCollider col1;
      [SerializeField] private CapsuleCollider col2;

      
      private static readonly int Right = Animator.StringToHash("Right");
      private static readonly int Left = Animator.StringToHash("Left");

      private bool isJumping;
      private static readonly int Jump1 = Animator.StringToHash("Jump");

      private void Awake()
      {
         Physics.IgnoreCollision(col2,col1,true);
      }
      
      private void Update()
      {
         if (isJumping) return;
         var colliders = Physics.OverlapSphere(overlapTransform.position, 0.5f, ballLayer);
      
         foreach (var coll in colliders)
         {
            isJumping = true;
            if (coll.gameObject.transform.position.y - transform.position.y >= 0.35f)
            {
               float distanceX;
               if(coll.gameObject.transform.position.x >= transform.position.x)
                   distanceX = coll.gameObject.transform.position.x - transform.position.x;
               else
               {
                  distanceX = coll.gameObject.transform.position.x - transform.position.x;
               }
               var distanceY = coll.gameObject.transform.position.y - transform.position.y;
               
               StartCoroutine(Jump(new Vector3(distanceX * 2,distanceY * 2,0)));
               animator.SetTrigger(Jump1);
            }
            else switch (coll.gameObject.transform.position.y - transform.position.y < 1)
            {
               case true when coll.gameObject.transform.position.x >= transform.position.x:
               {
                  var distance = coll.gameObject.transform.position.x - transform.position.x;
                  distance *= 2;
                  StartCoroutine(Jump(new Vector3(distance,0.3f,0)));
                  animator.SetTrigger(Right);
                  break;
               }
               case true when coll.gameObject.transform.position.x < transform.position.x:
               {
                  var distance = transform.position.x - coll.gameObject.transform.position.x;
                  StartCoroutine(Jump(new Vector3(-distance,0.3f,0)));
                  animator.SetTrigger(Left);
                  break;
               }
            }
         }
      }

      private IEnumerator Jump(Vector3 forceVector)
      {
         yield return new WaitForSeconds(0.1f);
         var step = 15f * Time.deltaTime;
         root.transform.position = Vector3.Lerp (transform.position, transform.position + forceVector, step);
      }

      public void GetDamage()
      {
         rb.isKinematic = false;

         col1.isTrigger = true;
         col2.isTrigger = false;

         StartCoroutine(AfterDamage());
         rb.AddForce(new Vector3(0,0,-1)* 3f, ForceMode.Impulse);
      }

      private IEnumerator AfterDamage()
      {
         yield return new WaitForSeconds(0.2f);
         rb.isKinematic = true;
         rb.velocity = Vector3.zero;

         col1.isTrigger = true;
         col2.isTrigger = false;
         root.transform.eulerAngles = new Vector3(0, 180, 0);
      }
   }
}
