using System.Collections;
using UnityEngine;

namespace Stickman
{
   public class StickmanGoalkeeper : MonoBehaviour
   {
      [SerializeField] private LayerMask ballLayer;
      [SerializeField] private Transform findPoint;
      [SerializeField] private Animator animator;
      [SerializeField] private GameObject root;
      private bool isJumping;
      private static readonly int CatchR = Animator.StringToHash("CatchRight");
      private static readonly int CatchL = Animator.StringToHash("CatchLeft");

      private void Update()
      {
         if (isJumping) return;
         var colliders = Physics.OverlapSphere(findPoint.position, 0.5f, ballLayer);
      
         foreach (var coll in colliders)
         {
            isJumping = true;
            if (coll.gameObject.transform.position.x >= transform.position.x)
            {
               var distance = coll.gameObject.transform.position.x - transform.position.x;
               StartCoroutine(Jump(new Vector3(distance,0,0)));
               animator.SetTrigger(CatchR);
            }
            else if (coll.gameObject.transform.position.x < transform.position.x)
            {
               var distance = transform.position.x - coll.gameObject.transform.position.x;
               StartCoroutine(Jump(new Vector3(-distance,0,0)));
               animator.SetTrigger(CatchL);
            }
         }
      }

      private IEnumerator Jump(Vector3 forceVector)
      {
         yield return new WaitForSeconds(0.1f);
         var step = 15f * Time.deltaTime;
         root.transform.position = Vector3.Lerp (transform.position, transform.position + forceVector, step);
        
      }
   }
}

