using System;
using System.Collections;
using Spline;
using UnityEngine;

public class ForceBarScript : MonoBehaviour
{
    [SerializeField] private GameObject root;
    public BezierCurve curve;
    public float duration;
    public bool lookForward;
    public SplineWalkerMode mode;
    public GameObject raycastPoint;

    private float progress;
    private bool goingForward = true;

    private enum State
    {
        NotWalk,
        Walk,
        Stand,
        End
       
    }

    private State currentState;
    
    private enum Control
    {
        None,
        Touch,
        Mouse
    }

    private Control currentControl;
    
    public delegate void ForceDelegate(float force);
    public ForceDelegate MMethodToCall;
    
    public event Action KickBall;

    private void Start()
    {
        currentControl = Control.None;
        currentState = State.NotWalk;
    }

    private void Update ()
    { 
        AndroidControl(); 
        EditorControl();
        
        if (currentState == State.NotWalk || currentState == State.End)
            return;

        ChangeState();
    }

    private void AndroidControl()
    {
        if(currentControl == Control.Mouse)
            return;
        if (Input.touchCount != 1) return;
        currentControl = Control.Touch;
        if (Input.touches[0].phase == TouchPhase.Began)
        {
            currentState = currentState switch
            {
                State.NotWalk => State.Walk,
                State.Walk => State.Stand,
                _ => currentState
            };
        }
    }
    private void EditorControl()
    {
        if (currentControl == Control.Touch)
            return;
        if (!Input.GetMouseButtonDown(0)) return;
        currentControl = Control.Mouse;
        currentState = currentState switch
        {
            State.NotWalk => State.Walk,
            State.Walk => State.Stand,
            _ => currentState
        };
    }

    private void ChangeState()
        {
            switch (currentState)
            {
                case State.Walk:
                {
                    if (goingForward)
                    {
                        progress += Time.deltaTime / duration;
                        if (progress > 1f)
                        {
                            switch (mode)
                            {
                                case SplineWalkerMode.Once:
                                    progress = 1f;
                                    break;
                                case SplineWalkerMode.Loop:
                                    progress -= 1f;
                                    break;
                                case  SplineWalkerMode.PingPong:
                                    progress = 2f - progress;
                                    goingForward = false;
                                    break;
                            }
                        }
                    }
                    else
                    {
                        progress -= Time.deltaTime / duration;
                        if (progress < 0f)
                        {
                            progress = -progress;
                            goingForward = true;
                        }
                    }

                    var position = curve.GetPoint(progress);
                    transform.localPosition = position;
                    if (lookForward)
                    {
                        transform.LookAt(position + curve.GetDirection(progress));
                    }

                    break;
                }
                case State.Stand:
                {
                    if (Physics.Raycast(raycastPoint.transform.position, transform.TransformDirection(Vector3.down),
                        out var hit, Mathf.Infinity))
                    {
                        switch (hit.transform.name)
                        {
                            case "Center":
                                MMethodToCall?.Invoke(5f);
                                break;
                            case "Green":
                                MMethodToCall?.Invoke(4f);
                                break;
                            case "Orange":
                                MMethodToCall?.Invoke(3f);
                                break;
                            case "Red":
                                MMethodToCall?.Invoke(2f);
                                break;
                        }
                    }
                    KickBall?.Invoke();
                    root.SetActive(false);
                    currentState = State.End;
                    break;
                }
            }
        }
    
}
