﻿namespace Spline
{
	public enum SplineWalkerMode {
		Once,
		Loop,
		PingPong
	}
}