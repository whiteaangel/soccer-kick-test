﻿using UnityEngine;
using UnityEngine.UI;

    public class UIText : MonoBehaviour
    {
        public void OnPointerEnter()
        {
            gameObject.GetComponent<Text>().fontSize += 5;
        }

        public void OnPointerExit()
        {
            gameObject.GetComponent<Text>().fontSize -= 5;
        }
    }

