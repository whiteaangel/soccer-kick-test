using UnityEngine;
using UnityEngine.SceneManagement;

namespace UI
{
    public class StartSceneController : MonoBehaviour
    {
        public void LoadScene(int sceneIndex)
        {
            SceneManager.LoadScene(sceneIndex);
        }

        public void ExitGame()
        {
            Application.Quit();
        }
   
    }
}
