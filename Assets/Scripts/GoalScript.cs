using System;
using UI;
using UnityEngine;
public class GoalScript : MonoBehaviour
{
   [SerializeField] private GameObject gamePanel;
   private void OnTriggerEnter(Collider other)
   {
      if (!other.CompareTag("Ball")) return;
      Time.timeScale = 0f;
      gamePanel.SetActive(true);
      gamePanel.GetComponent<GamePanel>().SetText("You wone!");
   }
   
}
